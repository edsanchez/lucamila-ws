<?php

return [
    //Estado de los usuarios
    'usuario_registrado'    => 0,
    'usuario_activo'        => 1,
    'usuario_inactivo'      => 2,

    //Estado de los enlaces de activación
    'enlace_activo'         => 0,
    'enlace_inactivo'       => 1,
    'enlace_vencido'        => 2,

    //Permisos
    'permiso_agregar'       => 1,
    'permiso_editar'        => 2,
    'permiso_consultar'     => 4,
    'permiso_eliminar'      => 8,

    //Tipo movimiento
    'movimiento_ingreso'    => 'ingreso',
    'movimiento_egreso'     => 'egreso',

    //Tipo presupuesto
    'presupuesto_usuario'   => 1,
    'presupuesto_grupo'     => 2,
    'presupuesto_cuenta'    => 3,

    //Roles en cuenta
    'rol_administrador'     => 'admin',
    'rol_colaborador'       => 'guest'
];
