<?php

return [
    //Estado de los usuarios
    'M1'    => serialize(array('estado' => 'true',  'tipo' => 'Información' , 'mensaje' => 'Cuenta activada correctamente')),
    'M2'    => serialize(array('estado' => 'false', 'tipo' => 'Error'       , 'mensaje' => 'El enlace ha vencido o se encuentra inactivo')),
    'M3'    => serialize(array('estado' => 'false', 'tipo' => 'Error'       , 'mensaje' => 'Error al activar la cuenta')),
    'M4'    => serialize(array('estado' => 'true',  'tipo' => 'Información' , 'mensaje' => 'La cuenta fue creada correctamente')),
    'M5'    => serialize(array('estado' => 'false', 'tipo' => 'Error'       , 'mensaje' => 'Hubo un error al crear la cuenta')),
    'M6'    => serialize(array('estado' => 'true',  'tipo' => 'Información' , 'mensaje' => 'La cuenta fue modificada correctamente')),
    'M7'    => serialize(array('estado' => 'false', 'tipo' => 'Error'       , 'mensaje' => 'No tiene permisos para modificar esta cuenta')),
    'M8'    => serialize(array('estado' => 'false', 'tipo' => 'Error'       , 'mensaje' => 'Tuvimos problemas para modificar la cuenta')),
    'M9'    => serialize(array('estado' => 'false', 'tipo' => 'Error'       , 'mensaje' => 'Parece que no tienes permiso para acceder a esta cuenta')),
    'M10'   => serialize(array('estado' => 'false', 'tipo' => 'Error'       , 'mensaje' => 'Hemos experimentado inconvenientes, vuelve a cargar la página')),
    'M11'   => serialize(array('estado' => 'false', 'tipo' => 'Error'       , 'mensaje' => 'Hemos experimentado inconvenientes al registrar la cuenta')),
    'M12'   => serialize(array('estado' => 'true',  'tipo' => 'Información' , 'mensaje' => 'La cuenta ha sido registrada, verifica tu bandeja de correo para activarla')),
    'M13'   => serialize(array('estado' => 'true',  'tipo' => 'Información' , 'mensaje' => 'Movimiento agregado correctamente')),
    'M14'   => serialize(array('estado' => 'false', 'tipo' => 'Error'       , 'mensaje' => 'Parece que no tienes permiso para agregar movimientos a esta cuenta')),
    'M15'   => serialize(array('estado' => 'true',  'tipo' => 'Información' , 'mensaje' => 'Movimiento modificado correctamente'))
];
