<?php

use Illuminate\Database\Seeder;

class CategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('categoria')->insert(array(
            ['nombre' => 'Salario', 'tipo' => 'ingreso', 'icon' => 'usd'],
            ['nombre' => 'Negocios', 'tipo' => 'ingreso', 'icon' => 'briefcase'],
            ['nombre' => 'Regalos', 'tipo' => 'ingreso', 'icon' => 'gift'],
            ['nombre' => 'Otros', 'tipo' => 'ingreso', 'icon' => 'money'],
        ));
        \DB::table('categoria')->insert(array(
            ['nombre' => 'Pasajes', 'tipo' => 'egreso', 'icon' => 'taxi'],
            ['nombre' => 'Comida', 'tipo' => 'egreso', 'icon' => 'cutlery'],
            ['nombre' => 'Bebidas', 'tipo' => 'egreso', 'icon' => 'glass'],
            ['nombre' => 'Compras', 'tipo' => 'egreso', 'icon' => 'shopping-cart'],
            ['nombre' => 'Vahículo', 'tipo' => 'egreso', 'icon' => 'car'],
            ['nombre' => 'Viajes', 'tipo' => 'egreso', 'icon' => 'plane'],
            ['nombre' => 'Familia/Amigos', 'tipo' => 'egreso', 'icon' => 'users'],
            ['nombre' => 'Entretenimiento', 'tipo' => 'egreso', 'icon' => 'ticket'],
            ['nombre' => 'Electricidad', 'tipo' => 'egreso', 'icon' => 'bolt'],
            ['nombre' => 'Agua', 'tipo' => 'egreso', 'icon' => 'tint'],
            ['nombre' => 'Hogar', 'tipo' => 'egreso', 'icon' => 'home'],
            ['nombre' => 'Ropa', 'tipo' => 'egreso', 'icon' => 'tag'],
            ['nombre' => 'Otros', 'tipo' => 'egreso', 'icon' => 'money']
        ));
    }
}
