<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('usuario')->insert([
            'nombres' => 'Edder Alaín',
            //'email' => str_random(10).'@gmail.com',
            'correo' => 'ed.0810@gmail.com',
            'password' => bcrypt('enter'),
            'estado'   => 1
        ]);
    }
}
