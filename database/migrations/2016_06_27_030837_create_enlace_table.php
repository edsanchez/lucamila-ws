<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnlaceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enlace', function(Blueprint $table){
            $table->increments('id');
            $table->string('enlace');
            $table->date('fecha_vencimiento');
            $table->integer('id_usuario')->unsigned();
            $table->integer('estado');
            $table->timestamps();

            $table->foreign('id_usuario')
                ->references('id')
                ->on('usuario');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('enlace');
    }
}
