<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeriodoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('periodo', function(Blueprint $table){
            $table->increments('id');
            $table->string('nombre');
            $table->date('fecha_inicio');
            $table->date('fecha_fin');
            $table->integer('id_cuenta')->unsigned();
            $table->integer('id_usuario')->unsigned();
            $table->softDeletes();

            $table->foreign('id_cuenta')
                ->references('id')
                ->on('cuenta');
            $table->foreign('id_usuario')
                ->references('id')
                ->on('usuario');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('periodo');
    }
}
