<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovimientoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movimiento', function(Blueprint $table){
            $table->increments('id');
            $table->enum('tipo', ['ingreso', 'egreso']);
            $table->decimal('monto', 10, 2);
            $table->date('fecha');
            $table->string('descripcion');
            $table->integer('id_categoria')->unsigned();
            $table->integer('id_rol')->unsigned();
            $table->integer('id_periodo')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_categoria')
                ->references('id')
                ->on('categoria');
            $table->foreign('id_rol')
                ->references('id')
                ->on('rol');
            $table->foreign('id_periodo')
                ->references('id')
                ->on('periodo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('movimiento');
    }
}
