<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rol', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_usuario')->unsigned();
            $table->integer('id_cuenta')->unsigned();
            $table->enum('rol', ['admin', 'guest']); //Administrador o invitado
            $table->integer('permiso')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_usuario')
                ->references('id')
                ->on('usuario');
            $table->foreign('id_cuenta')
                ->references('id')
                ->on('cuenta');
        });
        /**
         *Permiso:
         *1: Consultar  0001
         *2: Crear      0010
         *4: Editar     0100
         *5: Eliminar   1000
         */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rol');
    }
}
