<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemPresupuestoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_presupuesto', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_presupuesto')->unsigned();
            $table->integer('id_categoria')->unsigned();
            $table->decimal('monto', 10, 2);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_presupuesto')
                ->references('id')
                ->on('presupuesto');
            $table->foreign('id_categoria')
                ->references('id')
                ->on('categoria');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('item_presupuesto');
    }
}
