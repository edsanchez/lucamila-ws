<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Enlace;
use App\User;

class SeguridadController extends Controller
{
    public function activar($enlace_key){
        \DB::beginTransaction();
        try{
            $enlace = Enlace::where('enlace', '=', $enlace_key)
                ->first();
            if($enlace->estado == config('constantes.enlace_activo')){
                $usuario = User::find($enlace->id_usuario);
                $enlace->estado = config('constantes.enlace_inactivo');
                $enlace->save();

                $usuario->estado = config('constantes.usuario_activo');
                $usuario->save();
                //enviar correo
                $mensaje = array(
                    'nombre' => $usuario->nombres,
                    'email' => $usuario->correo,
                    'asunto' => 'Cuenta activada'
                );
                \Mail::send('emails.cuenta_activada',$mensaje, function ($message) use ($mensaje)
                {
                    //remitente
                    $message->from(env('CONTACT_MAIL'), env('CONTACT_NAME'));

                    //asunto
                    $message->subject($mensaje['asunto']);

                    //receptor
                    $message->to($mensaje['email'], $mensaje['nombre']);

                });
                \DB::commit();
                return response()->json(unserialize(config('mensajes.M1')));
            }
            return response()->json(unserialize(config('mensajes.M2')));
        }catch(\Exception $e){
            \DB::rollback();
            return response()->json(unserialize(config('mensajes.M3')));
        }
    }

    public static function setPermisos($permisos){
        $permiso = 0;
        for($i = 0; $i < count($permisos); $i++){
            $permiso = $permiso | $permisos[$i];
        }
        return $permiso;
    }

    public static function isPermiso($permiso, $accion){
        return ($permiso & $accion === $accion);
    }
}
