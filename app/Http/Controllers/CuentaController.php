<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Http\Requests;
use Crypt;

use App\Cuenta;
use App\Rol;
use App\Movimiento;

class CuentaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cuentas = \DB::table('cuenta')
            ->join('rol', 'cuenta.id', '=', 'rol.id_cuenta')
            ->whereRaw('rol.permiso & '.config('constantes.permiso_consultar').' = '. config('constantes.permiso_consultar'))
            ->where('rol.id_usuario', '=', \Auth::user()->id)
            ->select('cuenta.id', 'cuenta.nombre', 'cuenta.saldo_inicial', 'cuenta.saldo_actual')
            ->paginate(10);
        foreach($cuentas as $cuenta){
            $cuenta->id = Crypt::encrypt($cuenta->id);
        }
        return response()->json(['estado' => 'true', 'data' => $cuentas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \DB::beginTransaction();
        try{
            //Se crea la cuenta
            $input = $request->all();
            $cuenta = new Cuenta();
            $cuenta->nombre = $input['nombre'];
            $cuenta->id_usuario = \Auth::user()->id;
            $cuenta->saldo_inicial = $input['saldo_inicial'];
            $cuenta->saldo_actual = $input['saldo_inicial'];
            $cuenta->save();

            //Se crea el rol
            $rol = new Rol();
            $rol->id_usuario = \Auth::user()->id;
            $rol->id_cuenta = $cuenta->id;
            $rol->rol = 'admin';
            $rol->permiso = SeguridadController::setPermisos([
                config('constantes.permiso_agregar'),
                config('constantes.permiso_editar'),
                config('constantes.permiso_eliminar'),
                config('constantes.permiso_consultar')
            ]);
            $rol->save();
            \DB::commit();
            return response()->json(unserialize(config('mensajes.M4')));
        }catch(Exception $e){
            \DB::rollback();
            return response()->json(unserialize(config('mensajes.M5')));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_cuenta)
    {
        //Cuando se listan los movimientos de una cuenta:
        // Se verifica que el rol del usuario en sesión tenga permisos de consulta
        $id_cuenta = Crypt::decrypt($id_cuenta);
        try{
            $cuenta = Cuenta::select('id', 'nombre', 'saldo_inicial', 'saldo_actual')
                ->where('id', '=', $id_cuenta)
                ->firstOrFail();
            $cuenta->id = Crypt::encrypt($cuenta->id);
            $rol = Rol::where('id_cuenta', '=', $id_cuenta)
                ->where('rol.id_usuario', '=', \Auth::user()->id)
                ->whereRaw('rol.permiso & '.config('constantes.permiso_consultar').' = '. config('constantes.permiso_consultar'))
                ->firstOrFail();
            $cuenta->dias = Movimiento::select('fecha')
                ->selectRaw('SUM(monto) as monto')
                ->where('movimiento.id_rol', '=', $rol->id)
                ->groupBy('movimiento.fecha')
                ->orderBy('movimiento.fecha', 'desc')
                ->paginate(5);
            foreach($cuenta->dias as $dia){
                $dia->movimientos = \DB::table('movimiento')
                    ->select('movimiento.id'
                        , 'movimiento.tipo'
                        , 'movimiento.descripcion'
                        , 'movimiento.monto'
                        //, 'movimiento.fecha' fecha no porque está dentro de día
                        , 'categoria.icon'
                        , 'categoria.nombre as categoria')
                    ->join('categoria', 'categoria.id', '=', 'movimiento.id_categoria')
                    ->where('movimiento.id_rol', '=', $rol->id)
                    ->where('movimiento.fecha', '=', $dia->fecha)
                    ->orderBy('movimiento.fecha', 'desc')
                    ->orderBy('movimiento.created_at', 'desc')
                    ->get();
                foreach($dia->movimientos as $movimiento){
                    $movimiento->id = Crypt::encrypt($movimiento->id);
                }
            }
            return response()->json([
                'estado'    => 'true',
                'data'      => $cuenta
            ]);
        }catch(ModelNotFoundException $modelNotFoundException){
            return response()->json(unserialize(config('mensajes.M9')));
        }catch(Exception $e){
            return response()->json(unserialize(config('mensajes.M10')));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        \DB::beginTransaction();
        try{
            $id = Crypt::decrypt($id);
            $cuenta = Cuenta::find($id);
            $rol = Rol::where('id_usuario', '=', \Auth::user()->id)
            ->where('id_cuenta', '=', $cuenta->id)
            ->first();
            if(SeguridadController::isPermiso($rol->permiso, config('constantes.permiso_editar'))){
                $cuenta->nombre = $input['nombre'];
                $cuenta->save();
                \DB::commit();
                return response()->json(unserialize(config('mensajes.M6')));
            }else{
                \DB::rollback();
                return response()->json(unserialize(config('mensajes.M7')));
            }
        }catch(Exception $e){
            \DB::rollback();
            return response()->json(unserialize(config('mensajes.M8')));
        }
    }

    public function share(Request $request, $id){
        \DB::beginTransaction();
        try{
            //Faltan validaciones
            //El usuario no debe existir, el usuario que comparte debe ser admin en la cuenta

            $id_cuenta = Crypt::decrypt($id);
            $cuenta = Cuenta::find($id_cuenta);

            $usuario = User::where('correo', '=', $input['correo'])->first();

            //Se crea el rol
            $rol = new Rol();
            $rol->id_usuario = $usuario->id;
            $rol->id_cuenta = $cuenta->id;
            $rol->rol = config('constantes.rol_colaborador');
            $rol->permiso = SeguridadController::setPermisos([
                config('constantes.permiso_agregar'),
                config('constantes.permiso_editar'),
                config('constantes.permiso_consultar')
            ]);
            $rol->save();
            \DB::commit();
            return response()->json(unserialize(config('mensajes.M4')));
        }catch(Exception $e){
            \DB::rollback();
            return response()->json(unserialize(config('mensajes.M5')));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Confirmar si al eliminar se borrarán también todas sus dependencias
    }
}
