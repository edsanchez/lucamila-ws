<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Crypt;

use App\Http\Requests;
use App\Categoria;

class CategoriaController extends Controller
{
    public function getCategoriasIngreso(){
        $categorias = \DB::table('categoria')
            ->where('tipo', '=', config('constantes.movimiento_ingreso'))
            ->select('id', 'nombre', 'tipo', 'icon')
            ->get();
        foreach($categorias as $categoria){
            $categoria->id = Crypt::encrypt($categoria->id);
        }
        return response()->json(['estado' => 'true', 'data' => $categorias]);
    }

    public function getCategoriasEgreso(){
        $categorias = \DB::table('categoria')
            ->where('tipo', '=', config('constantes.movimiento_egreso'))
            ->select('id', 'nombre', 'tipo', 'icon')
            ->get();
        foreach($categorias as $categoria){
            $categoria->id = Crypt::encrypt($categoria->id);
        }
        return response()->json(['estado' => 'true', 'data' => $categorias]);
    }
}
