<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Crypt;
use App\Cuenta;
use App\Rol;
use App\Categoria;
use App\Movimiento;
use App\Http\Requests;

class MovimientoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id_cuenta)
    {
        \DB::beginTransaction();
        try{
            $id_cuenta = Crypt::decrypt($id_cuenta);
            $input = $request->all();
            $cuenta = Cuenta::findOrFail($id_cuenta);
            $rol = Rol::where('id_cuenta', '=', $id_cuenta)
                ->where('rol.id_usuario', '=', \Auth::user()->id)
                ->whereRaw('rol.permiso & '.config('constantes.permiso_agregar').' = '. config('constantes.permiso_agregar'))
                ->firstOrFail();
            // $id_categoria = Crypt::decrypt($input['id_categoria']);
            $id_categoria = $input['id_categoria'];
            $categoria = Categoria::findOrFail($id_categoria);
            $factor = $categoria->tipo == config('constantes.movimiento_ingreso') ? 1 : -1;

            $movimiento = new Movimiento();
            $movimiento->fecha = $input['fecha'];
            $movimiento->descripcion = $input['descripcion'];
            $movimiento->id_categoria = $categoria->id;
            $movimiento->tipo = $categoria->tipo;
            $movimiento->monto = abs($input['monto']) * $factor;
            $movimiento->id_rol = $rol->id;
            $movimiento->save();

            $cuenta->saldo_actual += $movimiento->monto;
            $cuenta->save();
            \DB::commit();

            return response()->json(unserialize(config('mensajes.M13')));
        }catch(ModelNotFoundException $modelNotFoundException){
            \DB::rollback();
            return response()->json(unserialize(config('mensajes.M14')));
        }catch(Exception $e){
            \DB::rollback();
            return response()->json(unserialize(config('mensajes.M10')));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_movimiento)
    {
        \DB::beginTransaction();
        try{
            $id_movimiento = Crypt::decrypt($id_movimiento);
            $input = $request->all();
            $movimiento = Movimiento::find($id_movimiento);
            $rol = Rol::where('id', '=', $movimiento->id_rol)
                ->where('rol.id_usuario', '=', \Auth::user()->id)
                ->whereRaw('rol.permiso & '.config('constantes.permiso_editar').' = '. config('constantes.permiso_editar'))
                ->firstOrFail();
            $last_monto = $movimiento->monto;
            // $id_categoria = Crypt::decrypt($input['id_categoria']);
            $id_categoria = $input['id_categoria'];
            $categoria = Categoria::findOrFail($id_categoria);
            $factor = $categoria->tipo == config('constantes.movimiento_ingreso') ? 1 : -1;

            $movimiento->tipo = $categoria->tipo;
            $movimiento->monto = abs($input['monto']) * $factor;;
            $movimiento->fecha = $input['fecha'];
            $movimiento->descripcion = $input['descripcion'];
            $movimiento->id_categoria = $id_categoria;
            $movimiento->save();

            $cuenta = Cuenta::find($rol->id_cuenta);
            $cuenta->saldo_actual += (($last_monto * -1) + $movimiento->monto);
            $cuenta->save();

            \DB::commit();
            return response()->json(unserialize(config('mensajes.M15')));
        }catch(Exception $ex){
            \DB::rollback();
            return response()->json(unserialize(config('mensajes.M10')));
        }catch(ModelNotFoundException $modelNotFoundException){
            \DB::rollback();
            return response()->json(unserialize(config('mensajes.M14')));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Confirmar si al eliminar se borrarán también todas sus dependencias
    }
}
