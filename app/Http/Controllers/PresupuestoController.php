<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Crypt;

use App\Http\Requests;
use App\Presupuesto;
use App\ItemPresupuesto;
use App\Cuenta;
use App\Usuario;
use App\Rol;
use App\Categoria;

class PresupuestoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \DB::beginTransaction();
        try{
            $input = $request->all();
            $usuario = Usuario::find(\Auth::user()->id);

            $presupuesto = new Presupuesto;
            $presupuesto->nombre = $input['nombre'];
            $presupuesto->descripcion = $input['descripcion'];
            $presupuesto->id_usuario = $usuario->id;
            $presupuesto->tipo = config('constantes.presupuesto_usuario');//$input['tipo'];
            $presupuesto->referencia = $usuario->id;//$input['referencia'];
            $presupuesto->save();

            //Se recorre lista de items
            foreach($input['items'] as $item){
                $categoria = Categoria::find(Crypt::decrypt($item['id_categoria']));

                $itemPresupuesto = new ItemPresupuesto;
                $itemPresupuesto->id_presupuesto = $presupuesto->id;
                $itemPresupuesto->id_categoria = $categoria->id;
                $itemPresupuesto->monto = $item['monto'];
                $itemPresupuesto->save();
            }
            \DB::commit();
        }catch(\Exception $e){
            \DB::rollback();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function control(){
        $presupuesto_first = \DB::table('categoria as c')
            ->leftJoin('item_presupuesto as ip', 'c.id', '=', 'ip.id_categoria')
            ->leftJoin('presupuesto as p', 'p.id', '=', 'ip.id_presupuesto')
            ->leftJoin('movimiento as m', 'c.id', '=', 'm.id_categoria')
            ->leftJoin('rol as r', 'r.id', '=', 'm.id_rol')
            ->where('c.tipo', '=', config('constantes.movimiento_egreso'))
            ->where(function($query){
                $query->where('p.id_usuario', '=', \Auth::user()->id)
                ->orWhereNull('p.id_usuario');
            })
            ->where(function($query){
                $query->where('r.id_usuario', '=', \Auth::user()->id)
                ->orWhereNull('r.id_usuario');
            })
            ->whereNull('ip.id')
            ->selectRaw("'No presupuestados' as categoria")
            ->selectRaw("'0' as presupuesto")
            ->selectRaw('abs(sum(m.monto)) as monto')
            ->selectRaw("'0' as factor")
            ->groupBy('categoria', 'presupuesto', 'factor');

        $presupuesto = \DB::table('categoria as c')
            ->leftJoin('item_presupuesto as ip', 'c.id', '=', 'ip.id_categoria')
            ->leftJoin('presupuesto as p', 'p.id', '=', 'ip.id_presupuesto')
            ->leftJoin('movimiento as m', 'c.id', '=', 'm.id_categoria')
            ->leftJoin('rol as r', 'r.id', '=', 'm.id_rol')
            ->where('c.tipo', '=', config('constantes.movimiento_egreso'))
            ->where(function($query){
                $query->where('p.id_usuario', '=', \Auth::user()->id)
                    ->orWhereNull('p.id_usuario');
            })
            ->where(function($query){
                $query->where('r.id_usuario', '=', \Auth::user()->id)
                    ->orWhereNull('r.id_usuario');
            })
            ->whereNotNull('ip.id')
            ->select('c.nombre as categoria', 'ip.monto as presupuesto')
            ->selectRaw('IF(ISNULL(abs(sum(m.monto))),0,abs(sum(m.monto))) as monto')
            ->selectRaw('IF(ISNULL(abs(sum(m.monto))),0,(abs(sum(m.monto)) / ip.monto) ) as factor')
            ->groupBy('categoria', 'presupuesto')
            ->union($presupuesto_first)
            ->orderBy('factor', 'desc')
            ->orderBy('presupuesto', 'desc')
            ->orderBy('monto', 'desc')
            ->get()
            ;

        return response()->json(['estado' => 'true', 'data' => $presupuesto]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
