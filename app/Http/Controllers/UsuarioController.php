<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Enlace;
use Carbon;

use Crypt;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function prueba(){
        return 'hola';
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\CreateUsuarioRequest $request)
    {
        \DB::beginTransaction();
        try{
            $input = $request->all();
            $usuario = new User;
            $usuario->nombres = $input['nombres'];
            $usuario->correo = $input['correo'];
            $usuario->password = \Hash::make($input['password']);
            $usuario->estado = config('constantes.usuario_registrado');
            $usuario->save();

            $enlace = new Enlace;
            $enlace->enlace = Crypt::encrypt(str_random(12));
            $enlace->id_usuario = $usuario->id;

            $hoy = new Carbon();
            $enlace->fecha_vencimiento = $hoy->addDay();
            $enlace->estado = config('constantes.enlace_activo');
            $enlace->save();
            \DB::commit();
        }catch(\Exception $e){
            \DB::rollback();
            return response()->json(unserialize(config('mensajes.M11')));
        }

        //enviar correo agregar tb la url fija
        $mensaje = array(
            'nombre' => $usuario->nombres,
            'email' => $usuario->correo,
            'enlace' => $enlace->enlace,
            'asunto' => 'Activa tu cuenta'
        );
        \Mail::send('emails.activacion',$mensaje, function ($message) use ($mensaje)
        {
            //remitente
            $message->from(env('CONTACT_MAIL'), env('CONTACT_NAME'));

            //asunto
            $message->subject($mensaje['asunto']);

            //receptor
            $message->to($mensaje['email'], $mensaje['nombre']);

        });
        return response()->json(unserialize(config('mensajes.M12')));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
