<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//Logeo
Route::post('login', ['as' =>'auth/login', 'uses' => 'Auth\AuthController@postLogin']);
Route::post('logout', 'Auth\AuthController@postLogout');

//Registro de usuario y activación
Route::get('activar/{enlace}', 'SeguridadController@activar');
Route::post('registrar', 'UsuarioController@store');

Route::group(['prefix' => 'cuentas', 'middleware' => ['auth', 'cors']], function(){
    Route::post('/', 'CuentaController@index');
    Route::post('agregar', 'CuentaController@store');
    Route::post('editar/{id}', 'CuentaController@update');
    Route::post('share/{id_cuenta}', 'CuentaController@share');
    Route::post('/{id}', 'CuentaController@show');
});
Route::group(['prefix' => 'movimiento', 'middleware' => ['auth', 'cors']], function(){
    Route::post('agregar/{id_cuenta}', 'MovimientoController@store');
    Route::post('update/{id}', 'MovimientoController@update');
});

Route::group(['prefix' => 'presupuesto', 'middleware' => ['auth', 'cors']], function(){
    Route::post('agregar', 'PresupuestoController@store');
    Route::post('control', 'PresupuestoController@control');
});

Route::group(['prefix' => 'categoria', 'middleware' => ['auth', 'cors']], function(){
    Route::post('ingreso', 'CategoriaController@getCategoriasIngreso');
    Route::post('egreso', 'CategoriaController@getCategoriasEgreso');
});
