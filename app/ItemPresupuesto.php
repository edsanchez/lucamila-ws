<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemPresupuesto extends Model
{
    use SoftDeletes;
    protected $table = 'item_presupuesto';
}
