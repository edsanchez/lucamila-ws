<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
        <h1>Cuenta activada!</h1>
        <p>
            Hola <span>{{ $nombre }}</span>, ya puedes acceder a la aplicación
        </p>
    </body>
</html>
