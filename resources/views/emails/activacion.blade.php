<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
        <h1>Activa tu cuenta!</h1>
        <p>
            Hola <span>{{ $nombre }}</span>, para activar tu cuenta ingresa al siguiente enlace:
        </p>
        <a href="{{ asset('activar/'.$enlace)}}">Activar cuenta</a>
    </body>
</html>
