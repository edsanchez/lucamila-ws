(
    select
        c.nombre as categoria,
        ip.monto as presupuesto,
        abs(sum(m.monto)) as monto,
        (abs(sum(m.monto)) / ip.monto) as factor
    from
        categoria as c
        left join item_presupuesto as ip
        on c.id = ip.id_categoria
        left join presupuesto as p
        on p.id = ip.id_presupuesto
        left join movimiento as m
        on c.id = m.id_categoria
        left join rol as r
        on r.id = m.id_rol
    where
        c.tipo = 'egreso' and (p.id_usuario = 1 OR p.id_usuario is null)
        and (r.id_usuario = 1 OR r.id_usuario is null) and ip.id is not null
    group by
        c.nombre,
        ip.monto
)
union
(
    select
        'No presupuestados' categoria,
        '0' presupuesto,
        abs(sum(m.monto)) as monto,
        '0' factor
    from
        categoria as c
        left join item_presupuesto as ip
        on c.id = ip.id_categoria
        left join presupuesto as p
        on p.id = ip.id_presupuesto
        left join movimiento as m
        on c.id = m.id_categoria
        left join rol as r
        on r.id = m.id_rol
    where
        c.tipo = 'egreso' and (p.id_usuario = 1 OR p.id_usuario is null)
        and (r.id_usuario = 1 OR r.id_usuario is null) and ip.id is null
    group by
        categoria,
        presupuesto,
        factor
)
order by
	factor desc,
	presupuesto DESC,
	monto desc
